﻿#include "myglwidget.h"
#include<QDebug>
#include<QFile>

static GLuint VBO,VAO,EBO;

MyGLWidget::MyGLWidget(QWidget *parent)
    : QOpenGLWidget (parent)
{
}

MyGLWidget::~MyGLWidget()
{
    glDeleteVertexArrays(1,&VAO);
    glDeleteBuffers(1,&VBO);
}

void MyGLWidget::initializeGL()
{
    this->initializeOpenGLFunctions();
    QString shaderContent=tr("#version 330 core\n"
                            "layout(location=0) in vec3 aPos;\n"
                            "void main(void)\n"
                            " {\n"
                            " gl_Position=vec4(aPos.x,aPos.y,aPos.z,1.0f);\n"
                            "}");

    bool success=shaderProgram.addShaderFromSourceCode(QOpenGLShader::Vertex,shaderContent);
   // bool success=shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,"E:/QTLab/QGLWidget/GLWidgetDemo/triangle.vert");
    if(!success){
        qDebug() << "shaderProgram addShaderFromSourceFile failed!" << shaderProgram.log();
        return;
    }
    success=shaderProgram.link();
    if(!success){
        qDebug() << "shaderProgram link failed!" << shaderProgram.log();
    }
    float vertices[]={
        0.5f,0.5f,0.0f,
        0.5f,-0.5f,0.0f,
        -0.5f,-0.5f,0.0f,
        -0.5f,0.5f,0.0f
    };
    unsigned int indices[]={
        0,1,3,
        1,2,3
    };
    glGenVertexArrays(1,&VAO);

    glGenBuffers(1,&VBO);
    glGenBuffers(1,&EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER,VBO);
    glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(indices),indices,GL_STATIC_DRAW);

    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,3*sizeof (GLfloat),(void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

}

void MyGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
}

void MyGLWidget::paintGL()
{

    glClearColor(0.2f,0.3f,0.3f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    shaderProgram.bind();
    glBindVertexArray(VAO);

    glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);

    shaderProgram.release();
}































