﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mychildwnd.h"
#include<QMessageBox>
#include<QFontDatabase>
#include<QSignalMapper>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initMainWindow();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::docOpen()
{
    QString docName=QFileDialog::getOpenFileName(this,u8"打开",QString(),u8"HTML 文档(*.htm *.html);;所有文件(*.*)");
    if(!docName.isEmpty())
    {
        QMdiSubWindow *exist=findChildWnd(docName);
        if(exist){
            ui->mdiArea->setActiveSubWindow(exist);
            return;
        }
        MyChildWnd *childwnd=new MyChildWnd;
        ui->mdiArea->addSubWindow(childwnd);
        connect(childwnd,SIGNAL(copyAvailable(bool)),ui->cutAction,SLOT(setEnabled(bool)));
        connect(childwnd,SIGNAL(copyAvailable(bool)),ui->copyAction,SLOT(setEnabled(bool)));
        if(childwnd->loadDoc(docName))
        {
            statusBar()->showMessage(u8"文档已打开",2000);
            childwnd->show();
            formatEnabled();
        }
        else {
            childwnd->close();
        }
    }
}

void MainWindow::docSave()
{
    if(activateChildWnd()&&activateChildWnd()->saveDoc())
        statusBar()->showMessage(u8"保存成功",2000);
}

void MainWindow::docSaveAs()
{
    if(activateChildWnd()&&activateChildWnd()->saveAsDoc())
        statusBar()->showMessage(u8"保存成功",2000);
}

void MainWindow::docUndo()
{
    if(activateChildWnd()) activateChildWnd()->undo();
}

void MainWindow::docRedo()
{
    if(activateChildWnd()) activateChildWnd()->redo();
}

void MainWindow::docCut()
{
    if(activateChildWnd()) activateChildWnd()->cut();
}

void MainWindow::docCopy()
{
    if(activateChildWnd()) activateChildWnd()->copy();
}

void MainWindow::docPaste()
{
    if(activateChildWnd()) activateChildWnd()->paste();
}

void MainWindow::docPrint()
{
    QPrinter pter(QPrinter::HighResolution);
    QPrintDialog *pdlg=new QPrintDialog(&pter,this);
    if(activateChildWnd()->textCursor().hasSelection())
        pdlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
    pdlg->setWindowTitle(u8"打印文档");
    if(pdlg->exec()==QDialog::Accepted)
        activateChildWnd()->print(&pter);
    delete  pdlg;
}

void MainWindow::docPrintPreview()
{
    QPrinter pter(QPrinter::HighResolution);
    QPrintPreviewDialog pview(&pter,this);
    connect(&pview,SIGNAL(paintRequested(QPrinter*)),SLOT(printPreview(QPrinter*)));
    pview.exec();
}

void MainWindow::printPreview(QPrinter *pter)
{
    activateChildWnd()->print(pter);
}

void MainWindow::textBold()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(ui->boldAction->isChecked()?QFont::Bold:QFont::Normal);
    if(activateChildWnd()) activateChildWnd()->setFormatOnSelectedWord(fmt);
}

void MainWindow::textItalic()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(ui->italicAction->isChecked());
    if(activateChildWnd()) activateChildWnd()->setFormatOnSelectedWord(fmt);
}

void MainWindow::textUnderline()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(ui->underlineAction->isChecked());
    if(activateChildWnd()) activateChildWnd()->setFormatOnSelectedWord(fmt);
}

void MainWindow::textFamily(const QString &fmly)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(fmly);
    if(activateChildWnd()) activateChildWnd()->setFormatOnSelectedWord(fmt);
}

void MainWindow::textSize(const QString &ps)
{
    qreal pointSize=ps.toFloat();
    if(ps.toFloat()>0)
    {
        QTextCharFormat fmt;
        fmt.setFontPointSize(pointSize);
        if(activateChildWnd()) activateChildWnd()->setFormatOnSelectedWord(fmt);
    }
}

void MainWindow::textColor()
{
    if(activateChildWnd())
    {
        QColor color=QColorDialog::getColor(activateChildWnd()->textColor(),this);
        if(!color.isValid()) return;
        QTextCharFormat fmt;
        fmt.setForeground(color);
        activateChildWnd()->setFormatOnSelectedWord(fmt);
        QPixmap pix(16,16);
        pix.fill(color);
        ui->colorAction->setIcon(pix);
    }
}

void MainWindow::paraStyle(int sidx)
{
    if(activateChildWnd()) activateChildWnd()->setParaStyle(sidx);
}

void MainWindow::on_aboutQtAction_triggered()
{
    QMessageBox::aboutQt(nullptr,u8"关于 Qt 5");
}

void MainWindow::on_aboutAction_triggered()
{
    QMessageBox::about(this,u8"关于",u8"这是一个基于Qt5实现的文字处理软件\r\n具备类似微软Office Word 的功能。");
}

void MainWindow::initMainWindow()
{
    QFontDatabase fontdb;
    foreach(int fontsize,fontdb.standardSizes())
        ui->sizeComboBox->addItem(QString::number(fontsize));
    ui->fontComboBox->addItems(fontdb.families());
    ui->sizeComboBox->setCurrentIndex(ui->sizeComboBox->findText(QString::number(QApplication::font().pointSize())));
    ui->mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    refreshMenus();
    connect(ui->mdiArea,SIGNAL(subWindowActivated(QMdiSubWindow*)),this,SLOT(refreshMenus()));
    myWndMapper=new QSignalMapper(this);
    connect(myWndMapper,SIGNAL(mapped(QWidget*)),this,SLOT(setActiveSubWindow(QWidget*)));
    addSubWndListMenu();
    connect(ui->menu_W,SIGNAL(aboutToShow()),this,SLOT(addSubWndListMenu()));

    QActionGroup *alignGroup=new QActionGroup(this);
    alignGroup->addAction(ui->leftAlignAction);
    alignGroup->addAction(ui->centerAction);
    alignGroup->addAction(ui->rightAlignAction);
    alignGroup->addAction(ui->justifyAction);
    ui->leftAlignAction->setChecked(true);

}

void MainWindow::docNew()
{
    MyChildWnd *childWnd=new MyChildWnd;
    ui->mdiArea->addSubWindow(childWnd);
    connect(childWnd,SIGNAL(copyAvailable(bool)),ui->cutAction,SLOT(setEnabled(bool)));
    connect(childWnd,SIGNAL(copyAvailable(boo)),ui->copyAction,SLOT(setEnabled(bool)));
    childWnd->newDoc();
    childWnd->show();
    formatEnabled();
}

void MainWindow::formatEnabled()
{
    ui->boldAction->setEnabled(true);
    ui->italicAction->setEnabled(true);
    ui->underlineAction->setEnabled(true);
    ui->leftAlignAction->setEnabled(true);
    ui->centerAction->setEnabled(true);
    ui->rightAlignAction->setEnabled(true);
    ui->justifyAction->setEnabled(true);
    ui->colorAction->setEnabled(true);
}

MyChildWnd *MainWindow::activateChildWnd()
{
    if(QMdiSubWindow *actSubWnd=ui->mdiArea->activeSubWindow())
        return qobject_cast<MyChildWnd *>(actSubWnd->widget());
    else
        return nullptr;
}

QMdiSubWindow *MainWindow::findChildWnd(const QString &docName)
{
    QString canonicalFilePath=QFileInfo(docName).canonicalFilePath();
    foreach(QMdiSubWindow *wnd,ui->mdiArea->subWindowList())
    {
        MyChildWnd *childwnd=qobject_cast<MyChildWnd *>(wnd->widget());
        if(childwnd->myCurDocPath==canonicalFilePath) return wnd;
    }
    return nullptr;
}

void MainWindow::textAlign(QAction *act)
{
    if(activateChildWnd())
    {
        if(act==ui->leftAlignAction)
            activateChildWnd()->setAlgignOfDocumentText(1);
        else if(act==ui->centerAction)
            activateChildWnd()->setAlgignOfDocumentText(2);
        else if(act==ui->rightAlignAction)
            activateChildWnd()->setAlgignOfDocumentText(3);
        else if(act==ui->justifyAction)
            activateChildWnd()->setAlgignOfDocumentText(4);
    }
}


void MainWindow::on_newAction_triggered()
{
    docNew();
}

void MainWindow::refreshMenus()
{
    bool hasChild=(activateChildWnd()!=nullptr);
    ui->saveAction->setEnabled(hasChild);
    ui->saveAction->setEnabled(hasChild);
    ui->printAction->setEnabled(hasChild);
    ui->printPreviewAction->setEnabled(hasChild);
    ui->pastAction->setEnabled(hasChild);
    ui->closeAction->setEnabled(hasChild);
    ui->closeAllAction->setEnabled(hasChild);
    ui->tileAction->setEnabled(hasChild);
    ui->cascadeAction->setEnabled(hasChild);
    ui->nextAction->setEnabled(hasChild);
    ui->previousAction->setEnabled(hasChild);
    bool hasSelect=(activateChildWnd() && activateChildWnd()->textCursor().hasSelection());
    ui->boldAction->setEnabled(hasSelect);
    ui->italicAction->setEnabled(hasSelect);
    ui->underlineAction->setEnabled(hasSelect);
    ui->leftAlignAction->setEnabled(hasSelect);
    ui->centerAction->setEnabled(hasSelect);
    ui->rightAlignAction->setEnabled(hasSelect);
    ui->justifyAction->setEnabled(hasSelect);
    ui->colorAction->setEnabled(hasSelect);
}

void MainWindow::addSubWndListMenu()
{
    ui->menu_W->clear();
    ui->menu_W->addAction(ui->closeAction);
    ui->menu_W->addAction(ui->closeAllAction);
    ui->menu_W->addSeparator();
    ui->menu_W->addAction(ui->tileAction);
    ui->menu_W->addAction(ui->cascadeAction);
    ui->menu_W->addSeparator();
    ui->menu_W->addAction(ui->nextAction);
    ui->menu_W->addAction(ui->previousAction);
    QList<QMdiSubWindow *>wnds=ui->mdiArea->subWindowList();
    if(!wnds.isEmpty()) ui->menu_W->addSeparator();
    for(int i=0;i<wnds.size();++i)
    {
        MyChildWnd *childwnd=qobject_cast<MyChildWnd *>(wnds.at(i)->widget());
        QString menuitem_text;
        if(i<9)
            menuitem_text=QString(u8"&%1 %2").arg(i+1).arg(childwnd->getCurDocName());
        else
            menuitem_text=QString(u8"%1 %2").arg(i+1).arg(childwnd->getCurDocName());
        QAction *menuitem_act=ui->menu_W->addAction(menuitem_text);
        menuitem_act->setCheckable(true);
        menuitem_act->setChecked(childwnd==activateChildWnd());
        connect(menuitem_act,SIGNAL(triggered()),myWndMapper,SLOT(map()));
        myWndMapper->setMapping(menuitem_act,wnds.at(i));
        formatEnabled();
    }

}

void MainWindow::on_closeAction_triggered()
{
    ui->mdiArea->closeActiveSubWindow();
}

void MainWindow::on_closeAllAction_triggered()
{
    ui->mdiArea->closeAllSubWindows();
}

void MainWindow::on_tileAction_triggered()
{
    ui->mdiArea->tileSubWindows();
}

void MainWindow::on_cascadeAction_triggered()
{
    ui->mdiArea->cascadeSubWindows();
}

void MainWindow::on_nextAction_triggered()
{
    ui->mdiArea->activateNextSubWindow();
}

void MainWindow::on_previousAction_triggered()
{
    ui->mdiArea->activatePreviousSubWindow();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    ui->mdiArea->closeAllSubWindows();
    if(ui->mdiArea->currentSubWindow())
        event->ignore();
    else
        event->accept();
}

void MainWindow::on_openAction_triggered()
{
    docOpen();
}

void MainWindow::on_saveAction_triggered()
{
    docSave();
}

void MainWindow::on_saveAsAction_triggered()
{
    docSaveAs();
}

void MainWindow::on_undoAction_triggered()
{
    docUndo();
}

void MainWindow::on_redoAction_triggered()
{
    docRedo();
}

void MainWindow::on_cutAction_triggered()
{
    docCut();
}

void MainWindow::on_copyAction_triggered()
{
    docCopy();
}

void MainWindow::on_pastAction_triggered()
{
    docPaste();
}

void MainWindow::on_boldAction_triggered()
{
    textBold();
}

void MainWindow::on_italicAction_triggered()
{
    textItalic();
}

void MainWindow::on_underlineAction_triggered()
{
    textUnderline();
}

void MainWindow::on_fontComboBox_activated(const QString &arg1)
{
    textFamily(arg1);
}

void MainWindow::on_sizeComboBox_activated(const QString &arg1)
{
    textSize(arg1);
}

void MainWindow::on_leftAlignAction_triggered()
{
    textAlign(ui->leftAlignAction);
}

void MainWindow::on_centerAction_triggered()
{
    textAlign(ui->centerAction);
}

void MainWindow::on_rightAlignAction_triggered()
{
    textAlign(ui->rightAlignAction);
}

void MainWindow::on_justifyAction_triggered()
{
    textAlign(ui->justifyAction);
}

void MainWindow::on_colorAction_triggered()
{
    textColor();
}

void MainWindow::on_styleComboBox_activated(int index)
{
    paraStyle(index);
}

void MainWindow::on_printAction_triggered()
{
    docPrint();
}

void MainWindow::on_printPreviewAction_triggered()
{
    docPrintPreview();
}
