﻿#include "dialog.h"
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QMessageBox>
#include "timeserver.h"
Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(u8"多线程时间服务器");
    Label1=new QLabel(u8"服务器端口:");
    Label2=new QLabel;
    quitBtn=new QPushButton(u8"退出");
    QHBoxLayout *BtnLayout=new QHBoxLayout;
    BtnLayout->addStretch(1);
    BtnLayout->addWidget(quitBtn);
    BtnLayout->addStrut(1);
    QVBoxLayout *mainLayout=new QVBoxLayout(this);
    mainLayout->addWidget(Label1);
    mainLayout->addWidget(Label2);
    mainLayout->addLayout(BtnLayout);
    connect(quitBtn,SIGNAL(clicked()),this,SLOT(close()));

    count=0;
    timeServer=new TimeServer(this);
    if(!timeServer->listen())
    {
        QMessageBox::critical(this,u8"多线程时间服务器",QString(u8"无法启动服务器：%1").arg(timeServer->errorString()));
        close();
        return;
    }
    Label1->setText(QString(u8"服务器端口：%1").arg(timeServer->serverPort()));
}

Dialog::~Dialog()
{

}

void Dialog::slotShow()
{
    Label2->setText(QString(u8"第%1次请求完毕").arg(++count));
}
