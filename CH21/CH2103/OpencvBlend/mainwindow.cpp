﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QPixmap>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initMainWindow();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initMainWindow()
{

    QString imgPath="shape01.jpg";
    Mat imgData=imread(imgPath.toLatin1().data());
    cvtColor(imgData,imgData,COLOR_BGR2RGB);
    myImg=imgData;
    myQImg=QImage((const unsigned char *)(imgData.data),imgData.cols,imgData.rows,QImage::Format_RGB888);
    imgShow();
}

void MainWindow::imgProc(float alp)
{
    Mat imgSrc1=myImg;
    QString imgPath="shape02.jpg";
    Mat imgSrc2=imread(imgPath.toLatin1().data());
    cvtColor(imgSrc2,imgSrc2,COLOR_BGR2RGB);
    Mat imgDst;
    addWeighted(imgSrc2,alp,imgSrc1,1-alp,0,imgDst);
    myQImg=QImage((const unsigned char *)(imgDst.data),imgDst.cols,imgDst.rows,QImage::Format_RGB888);
    imgShow();
}

void MainWindow::imgShow()
{
    ui->viewLabel->setPixmap(QPixmap::fromImage(myQImg.scaled(ui->viewLabel->size(),Qt::KeepAspectRatio)));
}

void MainWindow::on_verticalSlider_sliderMoved(int position)
{
    imgProc(position/100.0);
}
