﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "opencv2/opencv.hpp"
using namespace cv;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void initMainWindow();
    void imgProc();
    void imgShow();

private slots:


    void on_matchPushButton_clicked();

private:
    Ui::MainWindow *ui;
    Mat myImg;
    QImage myQImg;
};

#endif // MAINWINDOW_H
