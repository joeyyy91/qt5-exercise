﻿#include "mainwindow.h"
#include <QApplication>
#include<QPixmap>
#include<QSplashScreen>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPixmap pixMap("Qt.png");
    QSplashScreen splash(pixMap);
    splash.show();
    a.processEvents();
    MainWindow w;
    w.show();
    splash.finish(&w);
    return a.exec();
}
