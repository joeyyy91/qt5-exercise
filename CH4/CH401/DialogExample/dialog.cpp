﻿#include "dialog.h"
#include<QFileDialog>
#include<QColorDialog>
#include<QFontDialog>
#include<QMessageBox>
#pragma execution_character_set("UTF-8")
Palette::Palette(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(tr("各种标准对话框的实例"));
    fileBtn=new QPushButton;
    fileBtn->setText(tr("文件标准对话框实例"));
    fileLineEdit=new QLineEdit;

    mainLayout=new QGridLayout(this);
    mainLayout->addWidget(fileBtn,0,0);
    mainLayout->addWidget(fileLineEdit,0,1);
    connect(fileBtn,SIGNAL(clicked()),this,SLOT(showFile()));

    colorBtn=new QPushButton;
    colorBtn->setText(tr("颜色标准对话框实例"));
    colorFrame=new QFrame;
    colorFrame->setFrameShape(QFrame::Box);
    colorFrame->setAutoFillBackground(true);
    mainLayout->addWidget(colorBtn,1,0);
    mainLayout->addWidget(colorFrame,1,1);
    connect(colorBtn,SIGNAL(clicked()),this,SLOT(showColor()));

    fontBtn=new QPushButton;
    fontBtn->setText(tr("字体标准对话框实例"));
    fontLineEdit=new QLineEdit;
    fontLineEdit->setText(tr("Welcome!"));
    mainLayout->addWidget(fontBtn,2,0);
    mainLayout->addWidget(fontLineEdit,2,1);
    connect(fontBtn,SIGNAL(clicked()),this,SLOT(showFont()));

    inputBtn=new QPushButton;
    inputBtn->setText(tr("标准输入对话框实例"));
    mainLayout->addWidget(inputBtn,3,0);
    connect(inputBtn,SIGNAL(clicked()),this,SLOT(showInputDlg()));

    MsgBtn=new QPushButton;
    MsgBtn->setText(tr("标准消息对话框实例"));
    mainLayout->addWidget(MsgBtn,3,1);
    connect(MsgBtn,SIGNAL(clicked()),this,SLOT(showMsgDlg()));

    CustomBtn=new QPushButton;
    CustomBtn->setText(tr("用户自定义消息对话框实例"));
    label=new QLabel;
    label->setFrameStyle(QFrame::Panel|QFrame::Sunken);
    mainLayout->addWidget(CustomBtn,4,0);
    mainLayout->addWidget(label,4,1);
    connect(CustomBtn,SIGNAL(clicked()),this,SLOT(showCustomDlg()));
}

Palette::~Palette()
{

}

void Palette::showFile()
{
    QString s=QFileDialog::getOpenFileName(this,"open file dialog","/","C++ files(*.cpp);;C files(*.c);;Head files(*.h");
    fileLineEdit->setText(s);
}

void Palette::showColor()
{
    QColor c=QColorDialog::getColor(Qt::blue);
    if(c.isValid())
    {
        colorFrame->setPalette(QPalette(c));
    }
}

void Palette::showFont()
{
    bool ok;
    QFont f=QFontDialog::getFont(&ok);
    if(ok){
        fontLineEdit->setFont(f);
    }
}

void Palette::showInputDlg()
{
    inputDlg=new InputDlg(this);
    inputDlg->show();

}

void Palette::showMsgDlg()
{
    msgDlg=new MsgBoxDlg();
    msgDlg->show();
}

void Palette::showCustomDlg()
{
    label->setText(tr("Custom Message Box"));
    QMessageBox customMegBox;
    customMegBox.setWindowTitle(tr("用户自定义消息框"));
    QPushButton *yesBtn=customMegBox.addButton(tr("Yes"),QMessageBox::ActionRole);
     QPushButton *noBtn=customMegBox.addButton(tr("No"),QMessageBox::ActionRole);
     QPushButton *cancelBtn=customMegBox.addButton(QMessageBox::Cancel);
     customMegBox.setText(tr("这是一个用户自定义消息框"));
     customMegBox.setIconPixmap(QPixmap("Qt.png"));
     customMegBox.exec();
     if(customMegBox.clickedButton()==yesBtn)
         label->setText("Custom Message Box/Yes");
     else if(customMegBox.clickedButton()==noBtn)
         label->setText("Custom Message Box/No");
     else if(customMegBox.clickedButton()==cancelBtn)
         label->setText("Custom Message Box/Cancel");
     return;
}
